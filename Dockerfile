FROM microsoft/dotnet:sdk
MAINTAINER VoidCrafted <jay@jaywilliams.me>

WORKDIR /opt/

ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

RUN apt-get update
RUN apt-get install -y --force-yes redis-server
VOLUME /nadeko
WORKDIR /nadeko/src/NadekoBot
CMD dotnet run -c Release